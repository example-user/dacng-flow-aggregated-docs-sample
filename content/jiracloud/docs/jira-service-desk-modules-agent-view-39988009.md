---
title: Jira Service Desk Modules Agent View 39988009
---
# JIRA Service Desk modules - Agent view

This pages lists the JIRA Service Desk modules for the agent view. These can be used to inject new groups (tabs) in the JIRA Service Desk agent view.

**On this page:**

# Queues area

## Queue group

Description

A group of [serviceDeskQueues]

Module type

    serviceDeskQueueGroups

Screenshot

  Show me...

<img src="/jiracloud/files/39988009/sd-queue-content.png" class="confluence-embedded-image" /> 

Sample JSON

  Show me...

*![(warning)] Note that the group will not be displayed if there are no [serviceDeskQueues] that reference it in the "group" property or if all the queues that reference it are not displayed because of conditions.*

Properties

**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

 

**name**

-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

 

**`weight`**

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

 

**`conditions`**

-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

## Queue

Description

A queue in the queues sidebar

Module type

    serviceDeskQueues

Screenshot

  Show me...

<img src="/jiracloud/files/39988009/sd-queue-content.png" class="confluence-embedded-image" /> 

Sample JSON

  Show me...

Properties

**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**name**

-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

**`weight`**

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`group`**

-   **Type**: `string`
-   **Required**: no
-   **Description**: References the key of a [serviceDeskQueueGroup]. If this property is not provided, the queue will appear in a generic "Add-ons" group.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`conditions`**

-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

# Reports area

## Report group

Description

A group of [serviceDeskReports]

Module type

    serviceDeskReportGroups

Screenshot

  Show me...

<img src="/jiracloud/files/39988009/sd-report-content.png" class="confluence-embedded-image" />

Sample JSON

  Show me...

*![(warning)] Note that the group will not be displayed if there are no [serviceDeskReports] that reference it in the "group" property or if all the reports that reference it are not displayed because of conditions.*

Properties

**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

 

**name**

-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

 

**`weight`**

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

 

**`conditions`**

-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

## Report

Description

A report in the reports sidebar

Module type

    serviceDeskReports

Screenshot

  Show me...

<img src="/jiracloud/files/39988009/sd-report-content.png" class="confluence-embedded-image" />

Sample JSON

  Show me...

Properties

**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**name**

-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

**`weight`**

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`group`**

-   **Type**: `string`
-   **Required**: no
-   **Description**: References the key of a [serviceDeskReportGroup]. If this property is not provided, the report will appear in a generic "Add-ons" group.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`conditions`**

-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [serviceDeskQueues]: #JIRAServiceDeskmodules-Agentview-Queue
  [(warning)]: /s/en_GB/5989/aaad9997c145089d7f38b9dea0ac5b91728ef55a.56/_/images/icons/emoticons/warning.png
  [ `Single Condition` ]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html
  [ `Composite Condition` ]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html
  [Conditions]: https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html
  [serviceDeskQueueGroup]: #JIRAServiceDeskmodules-Agentview-Queuegroup
  [additional context]: /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
  [serviceDeskReports]: #JIRAServiceDeskmodules-Agentview-Report
  [serviceDeskReportGroup]: #JIRAServiceDeskmodules-Agentview-Reportgroup
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
