---
title: Jira Platform Modules 39987040
---
# JIRA platform modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows. 

JIRA Software and JIRA Service Desk also have their own application-specific modules (UI-related only). For more information, see the following pages:

-   [JIRA Software UI modules]
-   [JIRA Service Desk UI modules]

## Using JIRA modules

You can use a JIRA module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `generalPages` module at the `system.top.navigation.bar` location to your add-on, which adds a link in the JIRA header.

**atlassian-connect.json**

## JIRA platform modules

There are two types of modules: basic iframes that allow you to display content in different locations in JIRA, and more advanced modules that let you provide advanced JIRA-specific functionality.

### Basic modules 

-   [Dialog]: shows content inside of a modal dialog
-   [Page]: displays content on a full screen page within JIRA
-   [Web panel]: displays content in a panel (like on the View Issue screen)
-   [Web item]: adds a link or button to a defined location in JIRA (usually used on conjunction with dialogs or pages)
-   [Web section]: defines a new section to add multiple web items
-   [Webhook]: see the [Webhooks] page for more information

### Basic module locations

-   [JIRA platform modules - Administration console]
-   [JIRA platform modules - Project configuration]
-   [JIRA platform modules - User-accessible locations]
-   [JIRA platform modules - View issue page]

### Advanced modules for JIRA

These advanced modules provide access to pre-defined locations or features in JIRA. 

-   [Dashboard item]: provides a new gadget to display on JIRA dashboards
-   [Entity property]: see the [JIRA Entity properties][Entity property] page for more information
-   [Global permission]: defines a new global permission in JIRA
-   [Project admin tab pane]l: adds a new page to JIRA project settings
-   [Project permission]: defines a new project-level permission in JIRA
-   [Report]: adds a new type of report to the JIRA reports page
-   [Search request view]: renders a custom view of a search result that's accessible from the JIRA issue navigator
-   [Tab panel]: adds panels to the JIRA project sidebar, user profile page, or view issue page
-   [Workflow post-function]: adds a new post-function to a JIRA workflow

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [JIRA Software UI modules]: /jiracloud/jira-software-ui-modules-39987281.html
  [JIRA Service Desk UI modules]: /jiracloud/jira-service-desk-ui-modules-39988267.html
  [Dialog]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/dialog.html
  [Page]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/page.html
  [Web panel]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-panel.html
  [Web item]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-item.html
  [Web section]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-section.html
  [Webhook]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/webhook.html
  [Webhooks]: /jiracloud/webhooks-39987038.html
  [JIRA platform modules - Administration console]: /jiracloud/jira-platform-modules-administration-console-39988333.html
  [JIRA platform modules - Project configuration]: /jiracloud/jira-platform-modules-project-configuration-39988366.html
  [JIRA platform modules - User-accessible locations]: /jiracloud/jira-platform-modules-user-accessible-locations-39988374.html
  [JIRA platform modules - View issue page]: /jiracloud/jira-platform-modules-view-issue-page-39988380.html
  [Dashboard item]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/dashboard-item.html
  [Entity property]: /jiracloud/jira-entity-properties-39988397.html
  [Global permission]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/global-permission.html
  [Project admin tab pane]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-admin-tab-panel.html
  [Project permission]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-permission.html
  [Report]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/report.html
  [Search request view]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/search-request-view.html
  [Tab panel]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/tab-panel.html
  [Workflow post-function]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/workflow-post-function.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
