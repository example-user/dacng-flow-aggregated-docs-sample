---
title: Jira Service Desk Modules Web Item Target 40004227
---
# JIRA Service Desk modules - Web item target

Defines the way a web item link is opened in the browser, such as in a page or modal dialog.

# Example

# Properties

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><code>type</code></td>
<td><ul>
<li><strong>Type:</strong> <code>string</code></li>
<li><strong>Default:</strong> <code>page</code></li>
<li><strong>Allowed values:</strong>
<ul>
<li><code>page</code></li>
<li><code>dialog</code></li>
</ul></li>
<li><strong>Description:</strong> Defines how the web-item content should be loaded by the page. By default, the web-item is loaded in the same page. The target can be set to open the web-item url as a modal dialog</li>
</ul></td>
</tr>
</tbody>
</table>

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
