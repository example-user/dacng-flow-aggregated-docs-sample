---
title: Jira Service Desk Ui Modules 39988267
---
# JIRA Service Desk UI modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows.

The JIRA platform and JIRA Software also have their own modules. For more information, see the following pages:

-   [JIRA platform modules]
-   [JIRA Software UI modules]

## Using JIRA Service Desk UI modules

You can use a JIRA Service Desk UI module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `serviceDeskPortalHeaders` module to your add-on, which injects a panel at the top of customer portal pages in JIRA Service Desk.

**atlassian-connect.json**

## JIRA Service Desk UI modules

-   [JIRA Service Desk modules - Agent view]
-   [JIRA Service Desk modules - Customer portal]

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [JIRA platform modules]: /jiracloud/jira-platform-modules-39987040.html
  [JIRA Software UI modules]: /jiracloud/jira-software-ui-modules-39987281.html
  [JIRA Service Desk modules - Agent view]: /jiracloud/jira-service-desk-modules-agent-view-39988009.html
  [JIRA Service Desk modules - Customer portal]: /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
