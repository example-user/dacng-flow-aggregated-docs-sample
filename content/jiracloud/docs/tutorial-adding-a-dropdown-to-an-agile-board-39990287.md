---
title: Tutorial Adding A Dropdown To An Agile Board 39990287
---
# Tutorial - Adding a dropdown to an agile board

This tutorial will show you how to add a new dropdown to a board in JIRA Software, by using a JIRA Software module in your Connect add-on. This dropdown will appear in the top right corner of an Agile board, next to the **Board** dropdown.

To add this new dropdown, you will be using a JIRA Software module. To do this, you will create a new web-section at the location: `jira.agile.board.tools`.  You will then add a web-item or web-panel inside the web-section.

*Screenshot: Example web-section with a web-panel in jira.agile.board.tools.*

<img src="/jiracloud/files/39990287/Screen+Shot+2015-06-19+at+13.43.15.png" class="confluence-embedded-image" />

**About these Instructions**

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Sublime Text editor on Mac OS X. If you are using a different IDE and/or OS, you should use the equivalent operations for your specific environment.

This tutorial was last tested with JIRA 6.5-OD-07 and JIRA Agile 6.7.6. Note, the JIRA Software module used in this tutorial is only available in JIRA Agile 6.6.50 and later.

**
**

## Before you begin

-   If you haven't built a Connect add-on before, read the [Getting started] tutorial. You'll learn the fundamentals of Connect development and build a basic Connect add-on that you can use with this tutorial.

## Instructions

#### Step 1. Create a web-section and web-panel in your atlassian-connect.json

1.  Edit the **atlassian-connect.json** file in your add-on project.

2.  Add the following code for the `web-section` to the `modules` context:

3.  Add the following code for the `web-panel` to the `modules` context:

#### Step 2. Add server side code to render the web panel

The server-side code required to render the web panel is shown below. This code is for <a href="https://bitbucket.org/atlassian/atlassian-connect-express" class="external-link">atlassian-connect-express</a>, the node.js framework (with Express), but you can use a different technology stack if you wish.

1.  Navigate to the **routes** directory in your add-on project and edit the **index.js** file.
2.  Add the following code to the file and save it:

    This adds the 'configuration' route to your app.

3.  Navigate to the **views** directory in your add-on project and create a new **dropdown.hbs** file. 

4.  Add the following code to the **dropdown.hbs** file and save it:

#### Step 3. Check that it all works

1.  Start up and deploy your add-on (`npm start`) to your JIRA Cloud instance, if it's not running already. 
2.  Navigate to the board for any JIRA Software project. You should see a page like this:
    <img src="/jiracloud/files/39990287/Screen+Shot+2015-06-19+at+13.56.42.png" class="confluence-embedded-image" />

<img src="/s/en_GB/5989/aaad9997c145089d7f38b9dea0ac5b91728ef55a.56/_/images/icons/emoticons/check.png" alt="(tick)" class="emoticon emoticon-tick" />   **Congratulations!** You've just added a dropdown to an Agile board in your Connect add-on.

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Getting started]: /jiracloud/getting-started-39988011.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
