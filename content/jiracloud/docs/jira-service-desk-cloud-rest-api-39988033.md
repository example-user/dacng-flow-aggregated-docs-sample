---
title: Jira Service Desk Cloud Rest Api 39988033
---
# JIRA Service Desk Cloud REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. JIRA Service Desk Cloud provides a REST API for application-specific features, like queues and requests. Read the reference documentation below to get started.

#### <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link"><br />
</a>

#### <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link"><img src="/jiracloud/files/39988033/atlassian_software-51.png" class="confluence-embedded-image" /></a>

#### <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link">JIRA Service Desk REST API</a>

<sub>If\\ you\\ haven't\\ used\\ the\\ JIRA\\ REST\\ APIs\\ before,\\ make\\ sure\\ you\\ read\\ the [Atlassian\\ REST\\ API\\ policy].</sub>

## Other JIRA REST APIs

The JIRA Cloud platform provides a REST API for common features, like issues and workflows. 

-   [JIRA Cloud platform REST API]

JIRA Software Cloud also has its own REST API for application-specific features, like sprints and boards.

-   [JIRA Software Cloud REST API]

## Using the REST APIs

The best way to get started with the REST APIs is to jump into the reference documentation above. However, here are a few other resources to help you along the way.

### Authentication guides

The REST APIs support basic authentication, cookie-based (session) authentication, and OAuth. The following pages will get you started on using each of these authentication types with the REST APIs:

-   [Authentication and authorization] overview
-   [JIRA REST API - Basic authentication]
-   [JIRA REST API - Cookie-based Authentication]
-   [JIRA REST API - OAuth authentication]

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Atlassian\\ REST\\ API\\ policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Cloud platform REST API]: /jiracloud/jira-cloud-platform-rest-api-39987036.html
  [JIRA Software Cloud REST API]: /jiracloud/jira-software-cloud-rest-api-39988028.html
  [Authentication and authorization]: /jiracloud/authentication-and-authorization-39990700.html
  [JIRA REST API - Basic authentication]: /jiracloud/jira-rest-api-basic-authentication-39991466.html
  [JIRA REST API - Cookie-based Authentication]: /jiracloud/jira-rest-api-cookie-based-authentication-39991470.html
  [JIRA REST API - OAuth authentication]: /jiracloud/jira-rest-api-oauth-authentication-39991476.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
