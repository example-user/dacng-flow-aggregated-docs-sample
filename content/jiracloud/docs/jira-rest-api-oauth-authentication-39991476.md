---
title: Jira Rest Api Oauth Authentication 39991476
---
# JIRA REST API - OAuth authentication

This page shows you how to allow REST clients to authenticate themselves using <a href="https://en.wikipedia.org/wiki/OAuth" class="external-link">OAuth</a> token-based (1.0) authentication. This is one of three methods that you can use for authentication against the JIRA REST API; the other two being **[basic authentication]** and **[cookie-based authentication]**.

**On this page:**

## Overview

The instructions below describe how to use a Java client to provide OAuth authentication when making requests to JIRA's REST endpoints. It assumes you are familiar with the OAuth terminology (e.g. Consumer, Service Provider, request token, access token, etc.). For more information about OAuth refer to the OAuth specification.

**Looking for a Provider in a Language other than Java?**

Atlassian provides samples of OAuth providers in a number of other languages. <a href="https://bitbucket.org/atlassian_tutorial/atlassian-oauth-examples" class="external-link">Visit the sample repo on Bitbucket</a> to download and work with these samples.

## Step 1: Configuring JIRA

The first step is to register a new consumer in JIRA. This is done through the Application Links administration screens in JIRA. Create a new Application Link.
When creating the Application Link use a placeholder URL or the correct URL to your client, if your client can be reached via HTTP and choose the Generic Application type. After this Application Link has been created, edit the configuration and go to the incoming authentication configuration screen and select OAuth. Enter in this the public key and the consumer key which your client will use when making requests to JIRA.
After you have entered all the information click OK and ensure OAuth authentication is enabled.

## Step 2: Configuring the client

Your client will require the following information to be able to make authentication requests to JIRA.

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>OAuth Config</p></th>
<th><p>value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>request token url</p></td>
<td><p>JIRA_BASE_URL + &quot;/plugins/servlet/oauth/request-token&quot;</p></td>
</tr>
<tr class="even">
<td><p>authorization url</p></td>
<td><p>JIRA_BASE_URL + &quot;/plugins/servlet/oauth/authorize&quot;&quot;</p></td>
</tr>
<tr class="odd">
<td><p>access token url</p></td>
<td><p>JIRA_BASE_URL + &quot;/plugins/servlet/oauth/access-token&quot;</p></td>
</tr>
<tr class="even">
<td><p>oauth signing type</p></td>
<td><p>RSA-SHA1</p></td>
</tr>
<tr class="odd">
<td><p>consumer key</p></td>
<td><p>as configured in Step 1</p></td>
</tr>
</tbody>
</table>

In JIRA Cloud, the **Authorization URL** is only the Instance BASE\_URL: <a href="https://BASE_URL.atlassian.net" class="uri" class="external-link">https://BASE_URL.atlassian.net</a>. It also does not require the + "/plugins/servlet/oauth/authorize"

## Example Java OAuth client

This example java code demonstrates how to write a client to make requests to JIRA's rest endpoints using OAuth authentication.
To be able to use OAuth authentication the client application has to do the "OAuth dance" with JIRA. This dance consists of three parts.

1.  Obtain a request token
2.  Ask the user to authorize this request token
3.  Swap the request token for an access token

After the client application has a valid access token, this can be used to make authenticated requests to JIRA.

#### Before you begin

You'll need to configure JIRA and download the example client first. This example client uses the consumer key "hardcoded-consumer" and the public key is:

You have to create an Application Link as described in [Step 1] above and use this consumer key and the public key and leave the callback URL field empty.

Download the attached jar files:

| Name                                | Version | Date             |
|-------------------------------------|---------|------------------|
| [rest-oauth-client-1.0-sources.jar] | 1       | 2016-05-23 06:13 |
| [rest-oauth-client-1.0.one-jar.jar] | 1       | 2016-05-23 06:13 |

The **rest-oauth-client-1.0.one-jar.jar** contains the sample client and the **rest-oauth-client-1.0-sources.jar** contains the source code.

*![(warning)] If you are using JIRA 5.2 or earlier:** **The sample client uses HTTP POST to communicate with JIRA.  Support for OAuth parameters in the body of an HTTP POST was added in JIRA 6.0.  In order to run this sample client against a version of JIRA earlier than 6.0, the sample client (specifically `com.atlassian.oauth.client.example.AtlassianOAuthClient`) will need to be changed to use HTTP GET when communicating with JIRA.*

#### 1. Obtain a request token from JIRA

Execute this command:

Replace JIRA\_BASE\_URL with the URL to your JIRA instance and replace CALLBACK\_URL with the URL that should be called after the user has authorized the OAuth request token.
After executing this command you should see a response like

#### 2. Authorize this token

Go to the URL in system out and login into JIRA and approve the access. Afterwards JIRA will say that you have successfully authorised the access. It mentions a verification code which we need for the next step.

#### 3. Swap the request token with an access token

Execute the following command

Replace JIRA\_BASE\_URL, REQUEST\_TOKEN, TOKEN\_SECRET and VERIFIER with the correct values.

In the response you should see

This access token will allow you to make authenticated requests to JIRA.

#### 4. Make an authentication request to a rest-end point

To make an authenticated request to a rest resource in JIRA execute this command:

Replace ACCESS\_TOKEN, JIRA\_REST\_URL and ISSUE\_KEY with the correct values.
JIRA\_REST\_URL, e.g. <a href="http://localhost:8090/jira/rest/api/2/issue/HSP-1" class="uri" class="external-link">http://localhost:8090/jira/rest/api/2/issue/HSP-1</a>
This will return the issue JSON object for the issue with the key "HSP-1"

You should see a response like:

## Advanced topics

#### Using helper libraries

If you want to use OAuth to make requests to JIRA, the best way to do this is to find a **helper library** which takes care of signing the requests and reading the tokens from the response.
The example below is using the net.oauth library.

#### CAPTCHA

CAPTCHA is 'triggered' after several consecutive failed log in attempts, after which the user is required to interpret a distorted picture of a word and type that word into a text field with each subsequent log in attempt. If CAPTCHA has been triggered, you cannot use JIRA's REST API to authenticate with the JIRA site.

You can check this in the error response from JIRA — If there is an `X-Seraph-LoginReason` header with a a value of `AUTHENTICATION_DENIED`, this means the application rejected the login without even checking the password. This is the most common indication that JIRA's CAPTCHA feature has been triggered.

## Related pages

-   [JIRA REST API - Cookie-based Authentication][cookie-based authentication]
-   [JIRA REST API - Basic authentication][basic authentication]

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [basic authentication]: /jiracloud/jira-rest-api-basic-authentication-39991466.html
  [cookie-based authentication]: /jiracloud/jira-rest-api-cookie-based-authentication-39991470.html
  [Step 1]: #JIRARESTAPI-OAuthauthentication-step1
  [rest-oauth-client-1.0-sources.jar]: /jiracloud/files/39991476/rest-oauth-client-1.0-sources.jar
  [rest-oauth-client-1.0.one-jar.jar]: /jiracloud/files/39991476/rest-oauth-client-1.0.one-jar.jar
  [(warning)]: /s/en_GB/5989/aaad9997c145089d7f38b9dea0ac5b91728ef55a.56/_/images/icons/emoticons/warning.png
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
