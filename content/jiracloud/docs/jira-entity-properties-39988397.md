---
title: Jira Entity Properties 39988397
---
# JIRA entity properties

JIRA entity properties allow add-ons to add key/value stores to JIRA entities, such as issues or projects. These values can be indexed by JIRA and queried via a REST API or through JQL. This page provides an overview and examples of how entity properties work.

**On this page:**

## How do I store data against an entity using REST?

The <a href="http://curl.haxx.se/" class="external-link">curl</a>examples below show you how you can store data against an issue and retrieve the data from an issue, using REST. 

-   For information how to manipulate properties of other JIRA entities, e.g. projects, please see the <a href="https://docs.atlassian.com/jira/REST/latest/" class="external-link">JIRA REST API documentation</a>. 
-   P2 plugins can store/retrieve data using <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/bc/issue/properties/IssuePropertyService.html" class="external-link">IssuePropertyService</a>.

Please note, in order to modify, add or remove the properties, the user who is executing the request must have permission to edit the entity. For example, to add new property to issue ENPR-4, you need permission to edit the issue. In order to retrieve a property, the user must have read permissions for the entity.

#### Example 1: Storing data

The simple example below will store the JSON object **{"content":"Test if works on JIRA Cloud", "completed" : 1}** against issue ENPR-4 with the key **tasks. **

Note:

-   The **key** has a maximum length of 255 characters. 
-   The **value** must be a valid JSON Object and has a maximum size of 32 KB.

#### Example 2: Retrieving data

The example below shows how to get all of the properties stored against an issue.

The response from server will contain keys and URLs of all properties of the issue ENPR-4. 

#### Example 3: Removing a property

The example below shows how to remove a property from an issue.

## How do I make the properties of an entity searchable?

Atlassian Connect and P2 plugins both can provide a module descriptor, which will make the issue properties searchable using JQL. For example, to index the data from the first example, P2 and Connect plugins can provide the following module descriptors:

**P2 module descriptor**

**Connect module**

The descriptors shown above will make JIRA index the object **"content"** of the issue property **"tasks"** as a text. The available index data types are:

-   `number`: indexed as a number and allows for range ordering and searching on this field.
-   `text:` tokenized before indexing and allows for searching for particular words.
-   `string:` indexed as-is and allows for searching for the exact phrase only.
-   `date:` indexed as a date and allows for date range searching and ordering. The expected date format is \[YYYY\]-\[MM\]-\[DD\]. The expected date time format is \[YYYY\]-\[MM\]-\[DD\]T\[hh\]:\[mm\]:\[ss\] with optional offset from UTC: +/-\[hh\]:\[mm\] or `Z` for no offset. For reference, please see <a href="http://www.w3.org/TR/NOTE-datetime" class="external-link">ISO_8601 standard</a>.

The indexed data is available for a JQL search. The result of the JQL query: *issue.property\[tasks\].completed = 1 AND issue.property\[tasks\].content ~ "works" *is shown in the screenshot below. 

<img src="/jiracloud/files/39988397/jira-search-entityproperties.png" class="confluence-embedded-image" />

Please see the [Connect documentation] for detailed explanations on how to use the module descriptors. 

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Connect documentation]: https://developer.atlassian.com/static/connect/docs/modules/jira/entity-property.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
