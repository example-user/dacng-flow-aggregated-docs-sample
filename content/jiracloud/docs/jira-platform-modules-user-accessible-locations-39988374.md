---
title: Jira Platform Modules User Accessible Locations 39988374
---
# JIRA platform modules - User-accessible locations

This pages lists the JIRA platform modules for various locations in the JIRA UI that are accessible by non-admin users.

**On this page:**

## Top Navigation Bar Location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p></p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td> </td>
</tr>
<tr class="odd">
<td>Location key</td>
<td> </td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td>  Show me...
<p></p></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td>  Show me...
<pre><code> </code></pre></td>
</tr>
</tbody>
</table>

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
