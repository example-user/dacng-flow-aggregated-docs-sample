---
title: Guide Extending The Agent View Via The Jira Platform 39988302
---
# Guide - Extending the agent view via the JIRA platform

JIRA Service Desk is its own product, however it is built on top of the JIRA platform. This means that you can make use of the JIRA platform APIs and modules when extending JIRA Service Desk.

The agent view in JIRA Service Desk is actually a custom version of the standard JIRA issue view (unlike the customer portal, which is entirely unique to Service Desk). As a result, you can extend the agent view in many of the same ways that you extend the basic JIRA user interface.

 This guide will show you a few examples of how to extend the agent view by using the functionality of the JIRA platform and Atlassian Connect. 

**On this page:**

## Extending the agent view sidebar

The JIRA sidebar is the primary means by which agents will navigate through the agent view. With Atlassian Connect, you can extend the sidebar to include your own navigation items, allowing add-ons to provide a first-class experience for agent users.

To add an item to the sidebar, simply define a` jiraProjectTabPanel `module within your `atlassian-connect.json` descriptor as shown below. The given `name` will serve as both the text of the link within the sidebar, as well as the title of the page content.

<img src="https://extranet.atlassian.com/download/attachments/2798913789/jira-project-sidebar-link.png?version=1&amp;modificationDate=1462860776220&amp;api=v2" class="confluence-embedded-image" />

## Adding custom configuration pages

Within the agent view, the project settings area is where project administrators make use of JIRA Service Desk's extensive customisability in order to create the best solutions for their customers. Through Atlassian Connect, you can add your own configuration pages to the agent view, allowing add-ons to seamlessly integrate their configuration pages into the JIRA Service Desk UI.

To add a configuration page, simply define a `jiraProjectAdminTabPanel` module within your `atlassian-connect.json` descriptor as shown below. 

The valid locations include: projectgroup1, projectgroup2, projectgroup3, projectgroup4

<img src="https://extranet.atlassian.com/download/attachments/2798913789/jira-project-settings-sidebar-link.png?version=1&amp;modificationDate=1462860873698&amp;api=v2" class="confluence-embedded-image" />

## Extending the agent request view

Of all the areas in the agent view, the agent request view is perhaps the most central and common, serving as an operational hub for agents as they work with requests. As the agent request view is simply a JIRA issue view in disguise, you can leverage existing Atlassian Connect functionality to add to and extend various areas within the page. This allows add-ons to display additional request information, or even provide entirely new functionality to agent users.

As an example, to add a panel to the right-hand side of the agent request view, simply define a` webPanel` module within your `atlassian-connect.json` descriptor as shown below.

<img src="https://extranet.atlassian.com/download/attachments/2798913789/jira-issue-panel-content.png?version=2&amp;modificationDate=1463031589260&amp;api=v2" class="confluence-embedded-image" />

## Additional resources

For more information on extending the agent view, see the JIRA sections of the [Atlassian Connect documentation].

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Atlassian Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
