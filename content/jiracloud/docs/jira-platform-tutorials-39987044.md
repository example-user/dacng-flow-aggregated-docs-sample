---
title: Jira Platform Tutorials 39987044
---
# JIRA platform tutorials

The following pages will help you learn how to develop for the JIRA platform. Guides provide examples and best practices on JIRA concepts or processes. Tutorials provide a more hands-on approach to learning, by getting you to build something with step-by-step procedures.

Check them out below:

-   [Guide - Using the Issue Field module] — This guide will show you how to use the JIRA issue field module https://developer.atlassian.com/static/connect/docs/latest/modules/jira/issue-field.html. We'll take you on a guided tour of an example Connect add-on, jira-issue-field-demo, that uses the issue field module and explain the key concepts. 

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Guide - Using the Issue Field module]: /jiracloud/guide-using-the-issue-field-module-40501522.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
