---
title: Jira Software Context Parameters 39990789
---
# JIRA Software context parameters

Context parameters are special, context-specific values that can be supplied to your add-ons. These values are provided via special tokens, which are inserted into the URL properties of your [JIRA Software UI modules], specifically panels and actions.

Using context parameters, your Atlassian Connect add-ons can selectively alter their behaviour based on information provided by JIRA Software. Common examples include displaying alternate content, or even performing entirely different operations based on the available context.

## Using parameters

Context parameters can be used with any UI module that takes a URL property, as specified in your `atlassian-connect.json` descriptor. To use a context parameter, simply insert the corresponding key name, surrounded by curly-braces, at any location within your URL. The parameter will then be substituted into the URL as JIRA Software renders your UI module.

For example, the URL in the following snippet includes the `board.id` and `board.type` parameters:

## JIRA Software parameters

The following table details the list of context parameters provided by JIRA Software:

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th>Parameter key</th>
<th>Description</th>
<th>Available for</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>board.id</code></pre></td>
<td>The ID of the current board.</td>
<td><p></p></td>
</tr>
<tr class="even">
<td><pre><code>board.type</code></pre></td>
<td>The type (e.g. scrum, kanban) of the current request type.</td>
<td><p></p></td>
</tr>
<tr class="odd">
<td><pre><code>board.screen</code></pre></td>
<td>The screen of the current board.<br />
<em>Note, the old <code>board.mode</code> parameter was deprecated in favour of this parameter.</em></td>
<td><p>Modules that are displayed in multiple board screens.</p></td>
</tr>
<tr class="even">
<td><code>sprint.id</code></td>
<td>The ID of the current sprint.</td>
<td> </td>
</tr>
<tr class="odd">
<td><code>sprint.state</code></td>
<td>The state of the current sprint.</td>
<td> </td>
</tr>
</tbody>
</table>

## Additional parameters

The JIRA platform provides a number of additional context parameters that JIRA Software add-ons can use. For a full list of these context parameters, see the [Atlassian Connect documentation].

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [JIRA Software UI modules]: /jiracloud/jira-software-ui-modules-39987281.html
  [Atlassian Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/concepts/context-parameters.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
