---
title: Jira Cloud Development Platform 39981102
---
# JIRA Cloud development platform

<img src="/jiracloud/files/39981102/Connect+grahic.001.jpeg" class="confluence-embedded-image" />

#### Atlassian Connect is a framework in JIRA for building add-ons and integrations that integrate seamlessly, but have the flexibility and scalability of standalone web services.

Atlassian Connect uses [modules] that allow your add-on to plug in to the JIRA user interface. It provides a rich [JavaScript API] to make your add-on feel like part of the JIRA page, and simplifies installation, authorization, and authentication with the JIRA instance. Together with JIRA's [entity properties], [REST APIs], and [webhooks], the possibilities for what you can build are limitless.

## Your add-on

There are almost no limits on how your JIRA add-on or integration is deployed. From massive SaaS services to static apps served directly from a code repository, Atlassian Connect was designed to allow anyone to connect anything to Atlassian products.

 

<img src="/jiracloud/files/39981102/badges-12.png" class="confluence-embedded-image" />

**[Learn more about the Atlassian Connect architecture]**

## Your add-on descriptor

To take advantage of Atlassian Connect, your add-on must serve an [Atlassian Connect add-on descriptor]. The descriptor is a simple JSON file that is used to register your add-on with JIRA. It describes you and your add-on, handles installation lifecycle, authorization and authentication, and most importantly describes the **modules** that your add-on uses.

  Sample add-on descriptor

     {

         "key": "jira-activity",

         "name": "JIRA Project Activity",

         "description": "A Connect add-on that displays JIRA projects in a table",

         "vendor": {

             "name": "Atlassian Developer Relations",

             "url": "https://developer.atlassian.com/"

         },

         "baseUrl": "{{localBaseUrl}}",

         "links": {

             "self": "{{localBaseUrl}}/atlassian-connect.json",

             "homepage": "{{localBaseUrl}}/atlassian-connect.json"

         },

         "authentication": {

             "type": "jwt"

         },

         "lifecycle": {

             "installed": "/installed"

         },

         "scopes": [

             "READ"

         ],

          "modules": {

              "generalPages": [

                  {

                      "key": "activity",

                      "location": "system.top.navigation.bar",

                      "name": {

                          "value": "Activity"

                      },

                      "url": "/activity",

                      "conditions": [{

                          "condition": "user_is_logged_in"

                      }]

                  }

              ]

          }

     }

## Modules

Modules are the most important component of your JIRA add-on or integration. Simply put, these are the **integration points** that your add-on uses to provide rich interactions with JIRA. There are two types of modules: basic iframes that allow you to display content in [different places in JIRA], and more advanced modules that let you provide advanced JIRA-specific functionality.

<img src="/jiracloud/files/39981102/atlassian_documents-24.png" class="confluence-embedded-image" />

### Basic modules

These modules can be placed in a number of JIRA locations. [Learn more about UI module locations][different places in JIRA].

-   [**Dialog**]: show content inside of a modal dialog
-   [**Page**]: display content on a full screen page within JIRA
-   [**Web panel**]: display content in a panel (like on the View Issue screen)
-   [**Web item**]: add a link or button to a defined location in JIRA (usually used on conjunction with dialogs or pages)
-   [**Web section**]: define a new section to add multiple web items
-   [**Webhook**]: see the [section on webhooks below]

### Advanced modules for JIRA

These advanced modules provide access to pre-defined locations or features in JIRA.

-   [**Dashboard item**]: provide a new gadget to display on JIRA dashboards
-   [**Entity property**]: see the [section about storing data in JIRA below]
-   [**Global permission**]: define a new global permission in JIRA
-   [**Project admin tab pane**]l: add a new page to JIRA project settings
-   [**Project permission**]: define a new project-level permission in JIRA
-   [**Report**]: add a new type of report to the JIRA reports page
-   [**Search request view**]: render a custom view of a search result that's accessible from the JIRA issue navigator
-   [**Tab panel**]: add panels to the JIRA project sidebar, user profile page, or view issue page
-   [**Workflow post-function**]: add a new post-function to a JIRA workflow

## Store data in JIRA with entity properties

JIRA provides a powerful system for storing data with the JIRA host. There are three reasons why you might want to do this:

-   Simplify your add-on or integration by reducing how much data you store on your own server
-   Improve performance by evaluating `entity_property_equal_to` conditions in-process (learn more about conditions)
-   Implement advanced features like [JQL integration using search extractions]

<img src="/jiracloud/files/39981102/badges-12.png" class="confluence-embedded-image" />

**[Learn more about entity properties][**Entity property**]**

Entity properties are simple key-value stores attached to JIRA objects. Properties can be created, updated, and deleted via [global REST APIs]. Currently entity properties are available for:

-   Issues
-   Comments
-   Projects
-   Users
-   Issue types
-   Dashboard items

JIRA Software provides additional properties for [sprints] and [boards][sprints]. Atlassian Connect also provides **[add-on properties]** that are scoped specifically to your add-on and only accessible by you. This is a great place to store tenant-specific configuration data.

## REST APIs

For most communication with JIRA, you will use the JIRA REST APIs and webhooks. JIRA provides the [JIRA platform REST API], which covers most of the primary operations with basic JIRA objects like <a href="https://docs.atlassian.com/jira/REST/cloud/#api/2/issue" class="external-link">issues</a>, <a href="https://docs.atlassian.com/jira/REST/cloud/#api/2/project" class="external-link">projects</a>, <a href="https://docs.atlassian.com/jira/REST/cloud/#api/2/user" class="external-link">users</a>, <a href="https://docs.atlassian.com/jira/REST/cloud/#api/2/dashboard" class="external-link">dashboards</a>, <a href="https://docs.atlassian.com/jira/REST/cloud/#api/2/filter" class="external-link">filters</a>, and more. There are also product-specific REST APIs for [JIRA Software] and [JIRA Service Desk] for more advanced features like boards, backlogs, queues, SLAs and more.

## Webhooks

Webhooks are outgoing messages from JIRA that allow your add-on or integration to react to events, like someone transitioning an issue or closing a sprint. Like the REST APIs, there is a set of platform level webhooks and additional advanced webhooks for JIRA Software and JIRA Service Desk.

<img src="/jiracloud/files/39981102/badges-12.png" class="confluence-embedded-image" />

**[Learn more about webhooks]**

### <img src="/jiracloud/files/39981102/atlassian_documents-19.png" class="confluence-embedded-image" />Quick resources

-   <a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link">JIRA platform REST API</a>
-   <a href="https://docs.atlassian.com/jira-software/REST/cloud/" class="external-link">JIRA Software REST API</a>
-   <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link">JIRA Service Desk REST API</a>
-   [JIRA platform webhooks][Learn more about webhooks]
-   [JIRA Software webhooks]
-   [JIRA Service Desk Webhooks]
-   [Atlassian REST API Policy]

<a href="/jiracloud/integrate-with-the-jira-ui-using-atlassian-connect-39990073.html" class="aui-button aui-button-primary">Next: Integrate with the JIRA UI using Atlassian Connect</a>

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [modules]: #JIRAClouddevelopmentplatform-modules
  [JavaScript API]: #JIRAClouddevelopmentplatform-jsa
  [entity properties]: #JIRAClouddevelopmentplatform-entityprop
  [REST APIs]: #JIRAClouddevelopmentplatform-r
  [webhooks]: #JIRAClouddevelopmentplatform-w
  [Learn more about the Atlassian Connect architecture]: https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html
  [Atlassian Connect add-on descriptor]: https://developer.atlassian.com/static/connect/docs/latest/modules/
  [different places in JIRA]: /jiracloud/jira-platform-modules-39987040.html
  [**Dialog**]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/dialog.html
  [**Page**]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/page.html
  [**Web panel**]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-panel.html
  [**Web item**]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-item.html
  [**Web section**]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-section.html
  [**Webhook**]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/webhook.html
  [section on webhooks below]: #JIRAClouddevelopmentplatform-webhooks
  [**Dashboard item**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/dashboard-item.html
  [**Entity property**]: /jiracloud/jira-entity-properties-39988397.html
  [section about storing data in JIRA below]: #JIRAClouddevelopmentplatform-entityproperties
  [**Global permission**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/global-permission.html
  [**Project admin tab pane**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-admin-tab-panel.html
  [**Project permission**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-permission.html
  [**Report**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/report.html
  [**Search request view**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/search-request-view.html
  [**Tab panel**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/tab-panel.html
  [**Workflow post-function**]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/workflow-post-function.html
  [JQL integration using search extractions]: https://developer.atlassian.com/jiracloud/jira-entity-properties-39988397.html#JIRAentityproperties-HowdoImakethepropertiesofanentitysearchable?
  [global REST APIs]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/entity-property.html
  [sprints]: 
  [add-on properties]: https://developer.atlassian.com/static/connect/docs/latest/concepts/hosted-data-storage.html#add-on-properties
  [JIRA platform REST API]: /jiracloud/jira-cloud-platform-rest-api-39987036.html
  [JIRA Software]: /jiracloud/jira-software-cloud-rest-api-39988028.html
  [JIRA Service Desk]: /jiracloud/jira-service-desk-cloud-rest-api-39988033.html
  [Learn more about webhooks]: /jiracloud/webhooks-39987038.html
  [JIRA Software webhooks]: /jiracloud/jira-software-webhooks-39990316.html
  [JIRA Service Desk Webhooks]: /jiracloud/jira-service-desk-webhooks-39988324.html
  [Atlassian REST API Policy]: /display/HOME/Atlassian+REST+API+policy
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
