---
title: Jira Platform Modules Project Configuration 39988366
---
# JIRA platform modules - Project configuration

This pages lists the JIRA platform modules for the JIRA administration console. These can be used to inject menu sections and items in the Add-ons menu.

**On this page:**

## Project configuration menu

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Adds sections and items to the menu of the project configuration screen</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>atl.jira.proj.config</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td>  Show me...
<p><img src="/jiracloud/files/39988366/jira-projectconfig-menu.png" class="confluence-embedded-image" /></p></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td>  Show me...
<pre><code>...

&quot;modules&quot;: {

    &quot;webSections&quot;: [

        {

            &quot;key&quot;: &quot;example_menu_section&quot;,

            &quot;location&quot;: &quot;atl.jira.proj.config&quot;,

            &quot;name&quot;: {

                &quot;value&quot;: &quot;Example add-on name&quot;

            }

        }

    ],

    &quot;webItems&quot;: [

        {

            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {

                &quot;value&quot;: &quot;Example add-on link&quot;

            }</code></pre>
<pre><code>        }

    ],

}

...</code></pre></td>
</tr>
</tbody>
</table>

## Project summary area

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Adds panels to the project summary area</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>webpanels.admin.summary.left-panels</code> and <code>webpanels.admin.summary.right-panels</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td>  Show me...
<p><img src="/jiracloud/files/39988366/jira-projectconfig-summary.png" class="confluence-embedded-image" /></p></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td>  Show me...
<pre><code>...

&quot;modules&quot;: {

    &quot;webPanels&quot;: [

        {

            &quot;key&quot;: &quot;example_panel&quot;,

            &quot;location&quot;: &quot;webpanels.admin.summary.left-panels&quot;,

            &quot;name&quot;: {

                &quot;value&quot;: &quot;Example panel&quot;

            }

        }

    ],    </code></pre>
<pre><code>}

...</code></pre></td>
</tr>
</tbody>
</table>

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
