---
title: Jira Service Desk Context Parameters 39988330
---
# JIRA Service Desk context parameters

Context parameters are special, context-specific values that can be supplied to your add-ons. These values are provided via special tokens, which are inserted into the URL properties of your [JIRA Service Desk UI modules], specifically panels and actions.

Using context parameters, your Atlassian Connect add-ons can selectively alter their behaviour based on information provided by JIRA Service Desk. Common examples include displaying alternate content, or even performing entirely different operations based on the available context.

## Using parameters

Context parameters can be used with any UI module that takes a URL property, as specified in your `atlassian-connect.json` descriptor. To use a context parameter, simply insert the corresponding key name, surrounded by curly-braces, at any location within your URL. The parameter will then be substituted into the URL as JIRA Service Desk renders your UI module.

For example, the URL in the following snippet includes the `servicedesk.requestId` parameter:

## JIRA Service Desk parameters

The following table details the list of context parameters provided by JIRA Service Desk:

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th>Parameter key</th>
<th>Description</th>
<th>Available on</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>servicedesk.requestId</code></pre></td>
<td>The ID of the current customer request.</td>
<td><ul>
<li>Request details view</li>
</ul></td>
</tr>
<tr class="even">
<td><pre><code>servicedesk.requestTypeId</code></pre></td>
<td>The ID of the current request type.</td>
<td><ul>
<li>Create request view</li>
<li>Request details view</li>
</ul></td>
</tr>
<tr class="odd">
<td><pre><code>servicedesk.serviceDeskId</code></pre></td>
<td>The ID of the current service desk.</td>
<td><ul>
<li>Portal page</li>
<li>Create request view</li>
<li>Request details view</li>
</ul></td>
</tr>
</tbody>
</table>

## Additional parameters

The JIRA platform provides a number of additional context parameters that JIRA Service Desk Connect add-ons can use. For a full list of these context parameters, see the [Atlassian Connect documentation].

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [JIRA Service Desk UI modules]: /jiracloud/jira-service-desk-ui-modules-39988267.html
  [Atlassian Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/concepts/context-parameters.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
