---
title: Latest Updates 39988013
---
# Latest updates

We deploy updates to JIRA Cloud frequently. As a JIRA developer, it's important that you are aware of the changes. The resources below will help you keep track of what's happening:

[<img src="/jiracloud/files/39988013/atlassian_documents-12.png" class="confluence-embedded-image" />]

**[JIRA news][<img src="/jiracloud/files/39988013/atlassian_documents-12.png" class="confluence-embedded-image" />] **
*(Atlassian developer's blog)*

Major changes that affect JIRA Cloud developers are announced in this blog, e.g. deprecating APIs. You'll also find handy tips and articles related to JIRA development.

<a href="https://confluence.atlassian.com/display/Cloud/What%27s+New" class="external-link"><img src="/jiracloud/files/39988013/atlassian_documents-17.png" class="confluence-embedded-image" /></a>

**<a href="https://confluence.atlassian.com/display/Cloud/What%27s+New" class="external-link">What's new page</a> **
*(Atlassian Cloud documentation)*

This is the changelog for all Cloud applications, including changes that affect JIRA Cloud developers, e.g. the addition of new JIRA Software modules.

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [<img src="/jiracloud/files/39988013/atlassian_documents-12.png" class="confluence-embedded-image" />]: https://developer.atlassian.com/blog/categories/jira/
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
