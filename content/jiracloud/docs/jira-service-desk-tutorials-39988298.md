---
title: Jira Service Desk Tutorials 39988298
---
# JIRA Service Desk tutorials

The following pages will help you learn how to develop for JIRA Service Desk. Guides provide examples and best practices on JIRA Service Desk concepts or processes. Tutorials provide a more hands-on approach to learning, by getting you to build something with step-by-step procedures.

Check them out below:

-   [Guide - Exploring the JIRA Service Desk domain model via the REST APIs] — This guide will help you understand the JIRA Service Desk domain model, by using the JIRA Service Desk REST API https://docs.atlassian.com/jira-servicedesk/REST/latest/ and JIRA platform REST API https://docs.atlassian.com/jira/REST/latest/ to examine it.
-   [Guide - Creating JIRA Service Desk requests from Twitter] — This guide will show you how to integrate JIRA Service with Twitter to create requests from a Twitter stream. We'll take you on a guided tour of an example Connect add-on, jira-servicedesk-twitter-example, that implements this functionality and explain the key concepts.
-   [Guide - Extending the agent view via the JIRA platform] — This guide will show you a few examples of how to extend the agent view by using the functionality of the JIRA platform and Atlassian Connect.

There are also JIRA Service Desk tutorials in the Atlassian Connect documentation:

-   [Tutorial — My requests in JIRA Service Desk]: This tutorial will show you how to build a static Atlassian Connect add-on that displays the list of Service Desk requests made by the currently logged-in user, on a page accessible from the agent portal.

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Guide - Exploring the JIRA Service Desk domain model via the REST APIs]: /jiracloud/guide-exploring-the-jira-service-desk-domain-model-via-the-rest-apis-39990278.html
  [Guide - Creating JIRA Service Desk requests from Twitter]: /jiracloud/guide-creating-jira-service-desk-requests-from-twitter-40000561.html
  [Guide - Extending the agent view via the JIRA platform]: /jiracloud/guide-extending-the-agent-view-via-the-jira-platform-39988302.html
  [Tutorial — My requests in JIRA Service Desk]: https://developer.atlassian.com/static/connect/docs/latest/tutorials/my-requests-tutorial.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
