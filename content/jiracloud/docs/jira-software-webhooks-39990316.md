---
title: Jira Software Webhooks 39990316
---
# JIRA Software webhooks

JIRA Software webhooks are simply JIRA webhooks that are registered for JIRA Software events. JIRA Software webhooks are configured in the same way that you configure JIRA platform webhooks. You can register them via the JIRA administration console, you can add them as a post function to a workflow, you can inject variables into the webhook URL, etc. 

To learn more, read the JIRA platform documentation on webhooks: **[Webhooks]**

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Webhooks]: /jiracloud/webhooks-39987038.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
