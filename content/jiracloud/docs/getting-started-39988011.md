---
title: Getting Started (SOME GREAT UPDATE!)
---
# Getting started

Welcome to the JIRA Cloud developer documentation! This hands-on tutorial is designed to help you learn the basics of add-on development for the JIRA Cloud products (i.e. JIRA Core, JIRA Software, and JIRA Service Desk). 

It should take you about **10 minutes** to complete this tutorial. By the end of this tutorial, you'll have set up a local development environment, built a JIRA add-on, and tested it in a JIRA development instance.

**On this page:**

## Before you begin

To complete this tutorial, you'll need the following:

-   Your favourite IDE or text editor
-   A running node.js environment (v0.10.12 or later required). See the <a href="https://nodejs.org/en/download/" class="external-link">node.js downloads</a> page, if you need to install it.
-   A basic knowledge of JavaScript and web development

## Step 1. Set up your local development environment

Let's get a development environment set up on your local machine, so that we can start building add-ons! Add-ons for JIRA Cloud are built using the [Atlassian Connect framework]. You can write Connect add-ons in any programming language or framework, but in this tutorial you'll be using the node.js framework: <a href="https://bitbucket.org/atlassian/atlassian-connect-express" class="external-link">atlassian-connect-express</a> *(other frameworks are available [here])*.

1.  Install the **atlas-connect** CLI tool by running the following command:

2.  Test that atlas-connect has installed correct by checking the version:

    Your terminal should show something like this: `atlas-connect version 0.4.32.`

## Step 2. Get a JIRA Cloud instance

We've got you set up locally. Next, let's get a JIRA Cloud development instance that you can install your add-on in. 

1.  Go to <a href="http://go.atlassian.com/cloud-dev" class="external-link">go.atlassian.com/cloud-dev</a> and sign up for a free development environment.
2.  Once your Cloud instance is ready, sign in and complete the setup wizard. 
3.  On the Welcome screen, create a sample project and choose the 'Scrum software development' project.
4.  Finally, enable development mode for your Cloud instance. This lets you install add-ons in your instance that are not from the <a href="https://marketplace.atlassian.com/" class="external-link">Atlassian Marketplace</a>.
    1.  Navigate to JIRA administration (**cog icon** in the header) &gt; **Add-ons** &gt; **Manage add-ons**.
    2.  Scroll to the bottom of the 'Manage add-ons' page and click **Settings**.
    3.  Tick the **Enable development mode** checkbox.

## Step 3. Build an add-on

You now have a local development environment and a JIRA Cloud instance. We're ready to build an add-on! This add-on is going to be about as basic as it gets, but you'll be able to learn the fundamental steps of the process.

1.  Create an add-on project using the `atlas-connect` command. We're using the project name "jira-getting-started" in this tutorial:

    This command will generate the basic skeleton for your atlassian-connect-express enabled add-on, in a new **jira-getting-started** directory:

      Show me...

        .

        ├── README.md

        ├── app.js

        ├── atlassian-connect.json

        ├── config.json

        ├── credentials.json.sample

        ├── package.json

        ├── private-key.pem

        ├── public-key.pem

        ├── package.json

        ├── public

        │   ├── css

        │   │   └── addon.css

        │   └── js

        │       └── addon.js

        ├── routes

        │   └── index.js

        └── views

            ├── hello-world.hbs

            ├── layout.hbs

            └── unauthorized.hbs

2.  Change to the **jira-getting-started** directory and install all required dependencies:

That's it! You now have a Connect add-on. The `atlas-connect new` command actually creates a simple "Hello World" dialog in your new add-on, which you'll see when we deploy it to JIRA in the next step.

## Step 4. Deploy your add-on

You have a JIRA Cloud instance and you have a Connect add-on. It's time to put the two together.

1.  Next, copy the <span style="line-height: 1.42857;">**credentials.json.sample** file to a new </span>**credentials.json** file. Edit the **credentials.json **file and update the URL, username, and password to match your JIRA Cloud instance, then save it. It should look something like this:

    *Tip: The username is not the same as the email address you signed up with. The default username is "admin".*

2.  We're ready to deploy your add-on! Run the following command:

    This will boot up your Express server on the default port of 3000 and do the following:

    -   Create an ngrok tunnel to your local web server. <a href="https://ngrok.com/" class="external-link">Ngrok</a> is bundled with atlassian-connect-express. It is used to make your local add-on available to the internet, so that it can be installed in a JIRA Cloud instance. If you are not using atlassian-connect-express, you will have to install and set up a ngrok tunnel yourself by following these instructions: [Developing locally].
    -   Register your add-on's `atlassian-connect.json` (at `http://<temp-ngrok-url>.io/atlassian-connect.json`) with your host JIRA Cloud instance.
    -   Start watching for changes to your `atlassian-connect.json`. If the file is modified, `atlassian-connect-express` will re-register your add-on with the host.

3.  Finally, check that that your add-on is working correctly. Navigate to your JIRA Cloud instance and you'll see a **Hello World** link in the header. Click it and you should see a page like this:
    <img src="/jiracloud/files/39988011/jiradev-helloworld.png" class="confluence-embedded-image" />

**<img src="/s/en_GB/5989/aaad9997c145089d7f38b9dea0ac5b91728ef55a.56/_/images/icons/emoticons/check.png" alt="(tick)" class="emoticon emoticon-tick" /> Congratulations!** You've just built your first add-on for JIRA Cloud. Have a chocolate!

## Next steps

If you'd like to keep learning about add-on development for JIRA Cloud, the next step is to extend a JIRA application with an add-on. Try the JIRA Software tutorial linked below. You'll be able to take the basic add-on that you've already built, and follow the tutorial to add your own UI element: a custom board configuration page.

**[Tutorial - Adding a board configuration page]**

Here are some other things you might like to do next:

1.  Read the [JIRA Cloud development platform] overview *(recommended)* 
2.  If you are integrating with JIRA Software or JIRA Service Desk, check out these pages:
    -   [JIRA Software Cloud development]
    -   [JIRA Service Desk Cloud development]

3.  Tackle any of our other tutorials: [JIRA platform tutorials], [JIRA Software tutorials], [JIRA Service Desk tutorials]

## Resources

If you get stuck or want to give feedback, check out our support resources: [Support].

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Atlassian Connect framework]: https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html
  [here]: https://developer.atlassian.com/static/connect/docs/latest/developing/frameworks-and-tools.html
  [Developing locally]: https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally-ngrok.html
  [Tutorial - Adding a board configuration page]: /jiracloud/tutorial-adding-a-board-configuration-page-39990300.html
  [JIRA Cloud development platform]: /jiracloud/jira-cloud-development-platform-39981102.html
  [JIRA Software Cloud development]: /jiracloud/jira-software-cloud-development-39981104.html
  [JIRA Service Desk Cloud development]: /jiracloud/jira-service-desk-cloud-development-39981106.html
  [JIRA platform tutorials]: /jiracloud/jira-platform-tutorials-39987044.html
  [JIRA Software tutorials]: /jiracloud/jira-software-tutorials-39988385.html
  [JIRA Service Desk tutorials]: /jiracloud/jira-service-desk-tutorials-39988298.html
  [Support]: /jiracloud/support-39988134.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
