---
title: Jira Cloud Platform Rest Api 39987036
---
# JIRA Cloud platform REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. The JIRA Cloud platform provides a REST API for common features, like issues and workflows. Read the reference documentation below to get started.

<a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link"><br />
</a>

<a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link"><img src="/jiracloud/files/39987036/atlassian_objects-36.png" class="confluence-embedded-image" /></a>

#### <a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link">JIRA Cloud platform REST API reference</a>

<sub>*If\\ you\\ haven't\\ used\\ the\\ JIRA\\ REST\\ APIs\\ before,\\ make\\ sure\\ you\\ read\\ the [Atlassian\\ REST\\ API\\ policy].*</sub>

## Other JIRA REST APIs

The JIRA Software and JIRA Service Desk applications also have REST APIs for their application-specific features, like sprints (JIRA Software) or customer requests (JIRA Service Desk).

-   [JIRA Software Cloud REST API]
-   [JIRA Service Desk Cloud REST API]

## Using the REST APIs

The best way to get started with the JIRA Cloud platform REST API is to jump into the reference documentation. However, here are a few other resources to help you along the way.

### Authentication guides

The REST APIs support basic authentication, cookie-based (session) authentication, and OAuth. The following pages will get you started on using each of these authentication types with the REST APIs:

-   [JIRA REST API Example - Basic Authentication]
-   [JIRA REST API Example - Cookie-based Authentication]
-   [JIRA REST API Example - OAuth authentication]

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Atlassian\\ REST\\ API\\ policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Software Cloud REST API]: /jiracloud/jira-software-cloud-rest-api-39988028.html
  [JIRA Service Desk Cloud REST API]: /jiracloud/jira-service-desk-cloud-rest-api-39988033.html
  [JIRA REST API Example - Basic Authentication]: /jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-basic-authentication
  [JIRA REST API Example - Cookie-based Authentication]: /jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-cookie-based-authentication
  [JIRA REST API Example - OAuth authentication]: /jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-oauth-authentication
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
