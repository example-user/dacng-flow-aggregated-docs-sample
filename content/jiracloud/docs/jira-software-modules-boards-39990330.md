---
title: Jira Software Modules Boards 39990330
---
# JIRA Software modules - Boards

This pages lists the JIRA Software modules for boards. 

**On this page:**

## Board area

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Adds a dropdown menu to a board, next to the <strong>Boards</strong> menu.</td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webPanel</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>jira.agile.board.tools</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td>  Show me...
<p><img src="/jiracloud/files/39990330/jsw-board-menu.png" class="confluence-embedded-image" /></p></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td>  Show me...
<pre><code>...

&quot;modules&quot;: {

    &quot;webSections&quot;: [

        {

            &quot;key&quot;: &quot;board-links&quot;,

            &quot;location&quot;: &quot;jira.agile.board.tools&quot;,
         &quot;weight&quot;: 10,

            &quot;name&quot;: {

                &quot;value&quot;: &quot;My Extension&quot;

            }

        }

    ],   

    &quot;webPanels&quot;: [

        {

            &quot;key&quot;: &quot;my-web-panel&quot;,

            &quot;url&quot;: &quot;web-panel?id={board.id}&amp;mode={board.screen}&quot;,

            &quot;location&quot;: &quot;board-links&quot;,

            &quot;name&quot;: {

                &quot;value&quot;: &quot;My Web Panel&quot;

            },</code></pre>
<pre><code>         &quot;layout&quot;: {</code></pre>
<pre><code>             &quot;width&quot;: &quot;100px&quot;,</code></pre>
<pre><code>             &quot;height&quot;: &quot;100px&quot;</code></pre>
<pre><code>         }</code></pre>
<pre><code>        }

    ]

}

...</code></pre></td>
</tr>
</tbody>
</table>

## Board configuration

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Adds a board configuration page.</td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>jira.agile.board.configuration</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td>  Show me...
<p><img src="/jiracloud/files/39990330/jswdev-boardconfigpage.png" class="confluence-embedded-image" /></p></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td>  Show me...
<pre><code>...

&quot;modules&quot;: {       

    &quot;webPanels&quot;: [

        {

            &quot;key&quot;: &quot;my-configuration-page&quot;,

            &quot;url&quot;: &quot;configuration?id={board.id}&amp;type={board.type}&quot;,

            &quot;location&quot;: &quot;jira.agile.board.configuration&quot;,

            &quot;name&quot;: {

                &quot;value&quot;: &quot;My board configuration page&quot;

            },</code></pre>
<pre><code>         &quot;weight&quot;: 1

        }

    ]

}

...</code></pre></td>
</tr>
</tbody>
</table>

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
