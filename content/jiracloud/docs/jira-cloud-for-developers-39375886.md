---
title: Jira Cloud For Developers 39375886
---
# JIRA Cloud for Developers

## Build what's next for JIRA Cloud

##### Extend, enhance, and automate JIRA Software, JIRA Service Desk, and JIRA Core.

 

<img src="/jiracloud/files/39375886/atlassian_objects-11.png" class="confluence-embedded-image" />

### Add-ons *in* JIRA

[Atlassian Connect] is a framework for building immersive add-ons that feel like they are part of JIRA.

-   Display [content inside of JIRA issues]

-   [Add gadgets] to JIRA dashboards

-   [Extend the JIRA Service Desk customer portal]

<a href="/jiracloud/getting-started-39988011.html" class="aui-button aui-button-primary">Build your first JIRA Cloud add-on</a>

[Show me the API]

------------------------------------------------------------------------

## JIRA Cloud Platform

##### Atlassian Connect and JIRA Cloud are the foundation for add-ons for any JIRA product.

 

 <img src="/jiracloud/files/39375886/atlassian_objects-36+%281%29.png" class="confluence-embedded-image" />

[Atlassian Connect and JIRA overview]

<a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link">JIRA REST API</a>

[JIRA webhooks]

------------------------------------------------------------------------

## JIRA Cloud Products

### [<img src="/jiracloud/files/39375886/atlassian_software-13+%281%29.png" class="confluence-embedded-image" />]

### [JIRA Software Cloud][<img src="/jiracloud/files/39375886/atlassian_software-13+%281%29.png" class="confluence-embedded-image" />]

Build awesome add-ons for software teams with a dedicated API and webhooks for JIRA Software features like boards, backlogs, and estimation.

[JIRA Software overview for developers][<img src="/jiracloud/files/39375886/atlassian_software-13+%281%29.png" class="confluence-embedded-image" />]

<a href="https://docs.atlassian.com/jira-software/REST/cloud/" class="external-link">JIRA Software REST API</a>

[JIRA Software webhooks]

### [<img src="/jiracloud/files/39375886/atlassian_software-51.png" class="confluence-embedded-image" />]

### [JIRA Service Desk Cloud][<img src="/jiracloud/files/39375886/atlassian_software-51.png" class="confluence-embedded-image" />]

Build awesome add-ons for IT teams with a dedicated API and webhooks for JIRA Service Desk features like queues, SLAs, and the customer portal.

[JIRA Service Desk overview for developers][<img src="/jiracloud/files/39375886/atlassian_software-51.png" class="confluence-embedded-image" />]

<a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link">JIRA Service Desk REST API</a>

[JIRA Service Desk webhooks]

------------------------------------------------------------------------

## Get help

##### Our team is ready to help, don't hesitate to get in touch.

  <a href="https://design.atlassian.com/" class="external-link"><img src="/jiracloud/files/39375886/atlassian_documents-07+%281%29.png" class="confluence-embedded-image" /></a>

 

### Design

The <a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> shape the look and feel of our products. <a href="https://docs.atlassian.com/aui/latest/" class="external-link">Atlassian User Interface</a> is a front-end library to bring the ADG to life.

<a href="https://groups.google.com/forum/#!forum/atlassian-connect-dev" class="external-link"><img src="/jiracloud/files/39375886/atlassian_objects-28.png" class="confluence-embedded-image" /></a>

 

### Discuss

Join the conversation with fellow add-on developers <a href="https://groups.google.com/forum/#!forum/atlassian-connect-dev" class="external-link">in our Google Group</a>, or ask a question to the community on <a href="https://answers.atlassian.com/questions/topics/754005/atlassian-connect" class="external-link">Atlassian answers</a>.

<a href="https://ecosystem.atlassian.net/servicedesk/customer/portal/12" class="external-link"><img src="/jiracloud/files/39375886/atlassian_places-24.png" class="confluence-embedded-image" /></a>

 

### Support

Stumped? No worries.

-   <a href="https://ecosystem.atlassian.net/servicedesk/customer/portal/12" class="external-link">Submit a developer support request for JIRA Platform and JIRA Software</a>(or report a bug if you find one!)
-   Submit a JIRA Service Desk developer support request

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Atlassian Connect]: https://developer.atlassian.com/static/connect/docs/latest/index.html
  [content inside of JIRA issues]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-panel.html
  [Add gadgets]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/dashboard-item.html
  [Extend the JIRA Service Desk customer portal]: https://developer.atlassian.com/static/connect/docs/latest/tutorials/my-requests-tutorial.html
  [Show me the API]: https://developer.atlassian.com/jiracloud/jira-cloud-platform-rest-api-39987036.html
  [Atlassian Connect and JIRA overview]: /jiracloud/jira-cloud-development-platform-39981102.html
  [JIRA webhooks]: /jiracloud/webhooks-39987038.html
  [<img src="/jiracloud/files/39375886/atlassian_software-13+%281%29.png" class="confluence-embedded-image" />]: /jiracloud/jira-software-cloud-development-39981104.html
  [JIRA Software webhooks]: /jiracloud/jira-software-webhooks-39990316.html
  [<img src="/jiracloud/files/39375886/atlassian_software-51.png" class="confluence-embedded-image" />]: /jiracloud/jira-service-desk-cloud-development-39981106.html
  [JIRA Service Desk webhooks]: /jiracloud/jira-service-desk-webhooks-39988324.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
