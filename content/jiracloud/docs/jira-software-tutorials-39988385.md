---
title: Jira Software Tutorials 39988385
---
# JIRA Software tutorials

The following pages will help you learn how to develop for JIRA Software. Guides provide examples and best practices on JIRA Service Desk concepts or processes. Tutorials provide a more hands-on approach to learning, by getting you to build something with step-by-step procedures.

Check them out below:

-   [Tutorial - Adding a board configuration page] — This tutorial will show you how to build a board configuration page in JIRA Software, by using a JIRA Software module in your Connect add-on.
-   [Tutorial - Adding a dropdown to an agile board] — This tutorial will show you how to add a new dropdown to a board in JIRA Software, by using a JIRA Software module in your Connect add-on.

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [Tutorial - Adding a board configuration page]: /jiracloud/tutorial-adding-a-board-configuration-page-39990300.html
  [Tutorial - Adding a dropdown to an agile board]: /jiracloud/tutorial-adding-a-dropdown-to-an-agile-board-39990287.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
