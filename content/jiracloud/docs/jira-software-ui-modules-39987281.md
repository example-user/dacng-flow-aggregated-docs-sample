---
title: Jira Software Ui Modules 39987281
---
# JIRA Software UI modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows.

The JIRA platform and JIRA Service Desk also have their own modules. For more information, see the following pages:

-   [JIRA platform modules]
-   [JIRA Service Desk UI modules]

## Using JIRA Software UI modules

You can use a JIRA Software UI module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `webPanel` module at the `jira.agile.board.configuration` location in your add-on, which creates a new board configuration page in JIRA Software.

**atlassian-connect.json**

## JIRA Software UI modules

-   [JIRA Software modules - Boards]

Was this page helpful?

Help us improve - Report a problem with this page

[][] <a href="" class="a2a_button_facebook"></a> <a href="" class="a2a_button_twitter"></a> <a href="" class="a2a_button_google_plus"></a> <a href="" class="a2a_button_linkedin"></a> <a href="" class="a2a_button_reddit"></a> <a href="" class="a2a_button_hacker_news"></a>
### Have a question about this article?

<a href="https://answers.atlassian.com/tags/developer-documentation" id="aac-tags-link">See questions about this article</a>

Powered by [Confluence] and [Scroll Viewport]

  [JIRA platform modules]: /jiracloud/jira-platform-modules-39987040.html
  [JIRA Service Desk UI modules]: /jiracloud/jira-service-desk-ui-modules-39988267.html
  [JIRA Software modules - Boards]: /jiracloud/jira-software-modules-boards-39990330.html
  []: https://www.addtoany.com/share_save
  [Confluence]: http://atlassian.com/confluence/ "Atlassian Confluence"
  [Scroll Viewport]: https://www.k15t.com/software/scroll-viewport/overview "Scroll Viewport"
