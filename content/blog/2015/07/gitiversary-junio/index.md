---
title: "Happy Gitiversary Junio"
date: "2015-07-09T06:00:00+07:00"
author: "ssmith"
categories: ["git"]
---

<style>
  .center-image {
    text-align: center;
    margin: 1em;
    border-style: solid;
    border-color: #999999;
  }
  .right-image {
    float: right;
    margin: 1em;
  }
</style>

As Linus Torvalds has [already pointed out][torvalds-plus], the 8th of
July 2015 is the 10-year anniversary of [Junio C. Hamano][junio-blog]
taking on maintenance of [Git][git-web]. And here at Atlassian we
appreciate the excellent work he's done over that time.

For the last 10 years Junio has been nothing but an exemplary project
maintainer, taking Git from a niche tool for the Linux kernel to being
the industry-transforming platform it is now. Happy Gitiversary Junio,
and here's to many more years of maintainership from you!

**Update**: As [Junio himself has pointed out][junio-update], didn't
actually take over the maintainership of Git
[until the 27th][maint-msg]. However here at Atlassian we consider
Linus' excellent choice of successor to be worthy of celebration in
itself and we're happy to have two celebrations. ;)


[torvalds-plus]: https://plus.google.com/u/0/+LinusTorvalds/posts/MVj875h2UXJ
[junio-blog]: http://git-blame.blogspot.nl/
[git-web]: https://git-scm.com/
[junio-update]: https://plus.google.com/u/0/114877246484477251768/posts/PmTqRWgpzid
[maint-msg]: http://marc.info/?l=git&m=112243466603239
