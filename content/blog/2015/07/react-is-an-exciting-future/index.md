---
title: "React is an exciting future"
date: "2015-07-17T15:00:00+07:00"
author: "tsmith"
categories: ["React", "JavaScript"]
---

If [software is eating the world][software-eating], then JavaScript is eating software, and
[React][react] is eating JavaScript. In this food chain, it seems React has a big part of our
future &mdash; which is why I have been spending some time with it. Some interesting things have
surfaced...

## React is new[-ish]

At Atlassian, we use React internally and [externally][hipchat-react]. Perhaps we've gone a bit
[overboard with React][without-react] before, but with all new things it takes time to learn the
limitations.

We aren't alone in trying to learn React, I've seen people create fun stuff to learn, such as
[Tic-Tac-Toe][react-toe]. There's a slew of books coming out to help people learn React, like
[SurviveJS - Webpack and React][survivejs], [Master React][master-react], and
[Easy React][easy-react].

## Tooling

Having shipped products using different languages and platforms, I can empathize with thinking [Ruby
has terrible tools][ruby-tools]. I'm starting to believe that React won't be following the same
path, even now using [ES6 and ES7 features][react-on-es6] is possible with a bit of setup. Making
life easier. With [Redux][redux] &amp; [Webpack][webpack], it's possible to have
[hot reloading][live-react] via [react-hot-loader] and even rollbacks in your dev cycle. That's some
pretty impressive tooling that I'm excited to start using.

## Platform convergence

I like the suggestion that [mobile is "blowing up" because TV sucks][tv-sucks]. The web is easy (for
limited definitions of easy) but limited. This explosion of mobile and fracture of differing
platforms to support for client apps is a pain developers feel today. Without change, it will only
get worse. We are seeing technologies reflect that pain. The transition from writing web to client
apps can be easy with [React Native][react-native]. And tooling exists to support this,
[Nuclide][nuclide] is an IDE written to help work with React. Facebook based Nuclide on
[Electron][electron] so that they could reuse their expertise with web technologies to deliver a
desktop client app. Convergence exists even in the tools we use to help us with convergence. This
convergence of platforms is real and growing &mdash; Microsoft is part of this transition, with
universal apps for Windows 10, Windows Phone, and Xbox along with adaptor to make it possible to
support software written for other platforms.

<hr style="width: 80%; margin-left: auto; margin-right: auto;" />

React has that feeling of an exciting possible future for us as developers, along with growing
interest. React has good tooling (hot reloading, Nuclide, easy adoption of ES6/ES7 features). React
is deployable to web, mobile, and desktop allowing developers to reuse libraries between platforms.
React sure seems to have good reasons to have buzz around it. Even if React isn't the future, it has
learnings we can take on to the next phase of how we write software. It's exciting watching this
unfold.

[software-eating]: http://www.wsj.com/articles/SB10001424053111903480904576512250915629460 "Why Software Is Eating The World"
[react]: http://facebook.github.io/react/ "React: A JavaScript Library for Building User Interfaces"
[without-react]: /blog/2015/03/the-new-svg-hipchat-loading-screen-faster-without-react/ "The new SVG HipChat loading screen - faster without React"
[hipchat-react]: /blog/2015/02/rebuilding-hipchat-with-react/ "Rebuilding HipChat with React.js"
[survivejs]: http://survivejs.com/ "SurviveJS - Webpack and React"
[master-react]: http://ludovf.net/reactbook/ "Master React: Single Page Applications"
[easy-react]: http://easyreactbook.com "Easy React: Build Powerful Web Apps Using Modern JavaScript Technologies"
[react-toe]: https://github.com/reverentgeek/react-tac-toe "Tic-Tac-Toe in React"
[ruby-tools]: http://devblog.avdi.org/2015/07/08/ruby-is-defined-by-terrible-tools/ "Ruby is defined by terrible tools"
[react-on-es6]: http://babeljs.io/blog/2015/06/07/react-on-es6-plus/ "React on ES6+"
[live-react]: https://www.youtube.com/watch?v=xsSnOQynTHs "Dan Abramov - Live React: Hot Reloading with Time Travel at react-europe 2015"
[redux]: https://github.com/gaearon/redux
[webpack]: http://webpack.github.io/
[react-hot-loader]: https://github.com/gaearon/react-hot-loader
[tv-sucks]: http://toucharcade.com/2015/07/07/tv-sucks/ "Why is Mobile Gaming the Future, and Consoles are Dying? 'Well, Basically, the TV Sucks.'"
[react-native]: https://facebook.github.io/react-native/ "A Framework for Building Native Apps Using React"
[nuclide]: http://nuclide.io/ "An IDE for React Native"
[electron]: http://electron.atom.io/
