---
title: "Atlassian Connect for Bitbucket: A new way to extend your workflow in the cloud"
date: "2015-06-10T08:00:00+07:00"
author: "idelorenzo"
categories: ["atlassian-connect"]
---

<aside>
_This is a cross post from the [Bitbucket
blog](https://blog.bitbucket.org/2015/06/10/atlassian-connect-for-bitbucket-a-new-way-to-extend-your-workflow-in-the-cloud/)_
</aside>

More than 3 million developers and 450,000 teams use
[Bitbucket](http://bitbucket.org/) to manage and collaborate on source code. But
code collaboration is only a fraction of what software teams do on a daily basis
to ship software.

Nowadays, shipping great software involves constant context switching using
tools that don’t integrate tightly. Even when integrations are made, toolchains
have to be painstakingly maintained on an ongoing basis with reams of messy
bespoke integration code that has to be regularly re-written.

This is a challenge that development teams face every day. Most integration
architectures available right now in the software industry only partially solve
the problem. They only provide simple integration points that prevent teams from
creating deeper integrations and a more unified workflow.

### Not your average platform

To solve this problem, we’ve re-engineered Bitbucket into a platform that helps
remove the interrupt-driven workflow and brings all the information to ship
software in one place. [Atlassian Connect for
Bitbucket](http://developer.atlassian.com/bitbucket) provides an integration
architecture that embeds your add-ons right within the product UI creating a
more unified workflow.

Here is a demo of code search enabled by the add-on built by Sourcegraph:

<div class="video-player">
<iframe width="560" height="315" src="https://www.youtube.com/embed/sfopM3dNIg4"
frameborder="0" allowfullscreen></iframe>
</div>

### Why build using Atlassian Connect for Bitbucket?

If you are still wondering why you should build an add-on using Atlassian
Connect for Bitbucket, here are 3 reasons:

1. Atlassian Connect is a next generation extensibility framework, providing
 much deeper integration than what is offered by standard REST APIs and
 webhooks.
2. Add-ons are straightforward to implement. You can build an add-on in any
language, on any stack, using any interface.
3. You also can distribute your great add-ons to more than 3 million developers
and 450,000 teams using Bitbucket.

### Add-ons available for early preview

We couldn't launch an integration platform without the help of the community.
Here are previews of add-ons currently available on Bitbucket:

* Cloud IDE: Codeanywhere, Codio
* Code quality: bitHound, Codacy
* Code search: Sourcegraph
* Code analytics: StiltSoft
* Deployment: Aerobatic, CloudCannon, Platform.sh
* Crash and Exception Handling: Rollbar
* Rapid Integration: Wittified

_If you are currently a user of Bitbucket and want to take a sneak peak, click
on your avatar, select "Manage Account", and simply install these new add-ons by
selecting "[Find new add-ons](https://bitbucket.org/account/addon-directory/)"
from the left menu._

### Get started with Atlassian Connect for Bitbucket

It is now super-easy to extend Bitbucket so that you have the best workflow for
your team. [Click here for
documentation](http://developer.atlassian.com/bitbucket) to get started with
Atlassian Connect for Bitbucket.

We look forward to seeing what exciting and interesting add-ons you build using
Atlassian Connect for Bitbucket. Imagine the possibilities - happy developing!

<div class="signup-container"><a class="aui-button aui-button-primary" style="background:#14892c"
href="https://developer.atlassian.com/static/bitbucket/guides/getting-started.html">Get
started</a></div>

<style>
  .video-player {
    display: block;
    margin: 25px auto;
  }

  .video-player {
    width: 560px;
  }
</style>
