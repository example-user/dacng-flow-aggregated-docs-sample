---
title: "Stash plugin tutorial: tag list plugin"
date: "2015-03-26"
author: "tpettersen"
categories: ["git", "stash", "tutorial"]
---

<style>
  .video-player, .screen-caps {
    display: block;
    margin: 25px auto;
  }

  .video-player {
    width: 560px;
    text-align: center;
  }

  .big-image {
    width: 60%;
    margin: 0px auto;
  }

  .big-image img {
    border: 1px black solid;    
  }  
</style>

It's that time of year again! Time for Atlassian's San Francisco office to be 
insanely jealous of our counterparts down under. Specifically, we're jealous 
of the Sydney office's graduate program: the Atlassian *HackHouse*.

<div class="video-player">
  <iframe width="560" height="315" src="//www.youtube.com/embed/I5llRGYE3x4" 
          frameborder="0" allowfullscreen></iframe>  
</div>

For comparison, here's what the San Francisco office looks like during 
Australia's summer: 

<div class="video-player">
  <iframe width="560" height="315" src="//www.youtube.com/embed/bsn-Ykg21Jc" 
          frameborder="0" allowfullscreen></iframe>  
</div>

However, one awesome thing did shake out of this year's HackHouse that 
benefits everybody, no matter which hemisphere you live in! A member of the 
Stash development team wrote a neat Stash plugin tutorial for our grads to 
hack on. Presumably between mojitos. 

The [Stash Tag List Plugin][tag-list] is a self-contained Bitbucket repository 
that iteratively teaches you how to build a fully functional Stash plugin. In 
five simple steps you'll build a properly styled repository view that displays a 
list of tags in a Stash repository.

<div class="big-image">    
  <a href="step5.png"><img src="step5.png" alt="tag list repository view"></a>
</div>

The tutorial teaches you a range of useful plugin development skills, including:

- An understanding of basic Atlassian plugin anatomy 
- Using the SDK to build and deploy your plugin
- Registering servlets to build new Stash views
- Importing services and extracting data from the Stash Java API
- Constructing layout using Google Closure (Soy) Templates
- Binding javascript and other web resources to your views

To get started, you'll need a recent [JDK][jdk] and the 
[Atlassian SDK][atlas-sdk] installed. Then you can simply 
[clone the repository from Bitbucket][tag-list] and start from the README.md! 

If you're more of a visual learner, you can also check out 
[this screencast][beer] that teaches you how to build, deploy, and debug a Stash 
plugin from scratch in 30 minutes.

If you have any questions about Stash plugin development, leave a comment below 
or drop me a line on Twitter (I'm [@kannonboy][kannonboy]).

[tag-list]: https://bitbucket.org/atlassianlabs/stash-tag-list-plugin
[jdk]: http://www.oracle.com/technetwork/articles/javase/index-jsp-138363.html
[atlas-sdk]: https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project
[beer]: https://developer.atlassian.com/blog/2015/01/beer-o-clock-stash-plugin-tutorial/
[kannonboy]: https://twitter.com/kannonboy

