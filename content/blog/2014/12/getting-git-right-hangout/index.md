---
title: "Getting Git Right Hangout - December 9th"
date: "2014-12-03"
author: "tpettersen"
categories: ["git"]
---

We've just completed another round of the [Getting Git Right](https://www.atlassian.com/getting-git-right) 
tour, spreading the love about the hottest DVCS across ten cities in North America and Europe. To cap off 
the tour, the presenters are getting together for a special Google Hangout to talk about developer 
workflow and the latest happenings in the world of Git. 

<a href="https://plus.google.com/u/0/events/cqdbqud87e1ksrnsffgdpu7n9q4">
  <img alt="Dev Den Office Hours" src="dev_den_office_hours.png" style="display: block;
margin-right:auto;margin-left:auto;margin-top:30px;margin-bottom:30px;">
</a>

Topics to be discussed are:

- why we're excited about the [recent Git 2.2.0 release](https://developer.atlassian.com/blog/2014/12/git-2-2-0-released/)
- the latest content from the Getting Git Right tour
- results from "ask the audience" polls on developer best practices in cities across Europe and North 
America
- popular questions from the Getting Git Right audience
- additional questions from our recent webinars on 
[JIRA & Stash integration](https://www.youtube.com/watch?v=M-u8-Ga6if0), 
[Bamboo & Git](https://www.youtube.com/watch?v=wSASqmzBaAs), and 
[Git workflows for SaaS teams](https://www.youtube.com/watch?v=O4SoB3TFkjA)

There'll be plenty of time for questions from the audience, so please 
[join us on December 9th](https://plus.google.com/u/0/events/cqdbqud87e1ksrnsffgdpu7n9q4) with your Git 
gripes (or good words), workflow woes, and anything else you'd like to hear from Atlassian's Git experts: 
[@durdn](https://twitter.com/durdn), [@tarkasteve](https://twitter.com/tarkasteve), [
@devpartisan](https://twitter.com/devpartisan), [@GraceFr](https://twitter.com/GraceFr), and myself 
([@kannonboy](https://twitter.com/kannonboy)). Feel free to post questions in advance on 
[our Google+ page](https://plus.google.com/u/0/events/cqdbqud87e1ksrnsffgdpu7n9q4).

If you couldn't come along to the Getting Git Right tour, we've posted 
[a recording of the Seattle event](https://www.youtube.com/watch?v=sevIoGuuJMo) on YouTube. Enjoy!