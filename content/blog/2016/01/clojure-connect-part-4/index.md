---
title: "Let's build a Bitbucket add-on in Clojure! - Part 4: Talking to Bitbucket"
date: "2016-01-14T06:00:00+07:00"
author: "ssmith"
description: "Part 3 of the developing Atlassian Connect Bitbucket integration in Clojure. This installment covers communicating with Bitbucket via REST and JavaScript."
categories: ["bitbucket", "atlassian-connect", "clojure", "bbclojureseries"]
---

<style>
  .float-image {
      display: block;
      margin: 15px auto 30px auto;
  }
  .shadow-image {
      display: block;
      margin: 15px auto 30px auto;
      box-shadow: 10px 10px 15px 5px #888888;
  }
</style>


In [part 3 of this series][part3] we added REST and JSON capabilities to our
add-on. However most Atlassian Connect add-ons will want to add some
user-interface elements to the Bitbucket repository too, usually by working with
data from the repository. To get this data, the add-on will need to talk to
Bitbucket directly. In this installment, we'll look at a couple of ways to do
this, including how to authenticate using the handshake information we received
in [the previous blog post][part3].

## Talking to Bitbucket

The real power of Atlassian Connect is the ability to modify Bitbucket's UI to
expand its functionality. However for most add-ons this will entail
communicating with Bitbucket via the API to interact with a user's repository
and other information. This is why in the previous installment when we
implemented the calls that Bitbucket will make back to our add-on we left out
the `/connect-example` endpoint which serves up the HTML component that will be
embedded into Bitbucket. Before we can fill out this endpoint we need to be able
to fetch some metadata about the repository from Bitbucket.

### Two methods: Browser or server-side calls

The Bitbucket API can be accessed via two methods; server-side calls, or from
the browser UI component itself via JavaScript. We'll use both in our example,
so let's define our UI component template in
`resources/views/connect-example.selmer`:

``` html
<!doctype html>
<html>
  <head>
    <title>Atlassian Connect</title>

    <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.6.11/css/aui.css" media="all">
    <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.6.11/css/aui-experimental.css" media="all">
    <!--[if IE 9]><link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.6.11/css/aui-ie9.css" media="all"><![endif]-->
    <link rel="stylesheet" href="/css/addon.css" type="text/css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="//aui-cdn.atlassian.com/aui-adg/5.6.11/js/aui-soy.js" type="text/javascript"></script>
    <script src="//aui-cdn.atlassian.com/aui-adg/5.6.11/js/aui.js" type="text/javascript"></script>
    <script src="//aui-cdn.atlassian.com/aui-adg/5.6.11/js/aui-datepicker.js"></script>
    <script src="//aui-cdn.atlassian.com/aui-adg/5.6.11/js/aui-experimental.js"></script>

    <script src="https://bitbucket.org/atlassian-connect/all-debug.js" type="text/javascript"></script>
  </head>

  <body class="aui-page-hybrid">
    <section id="content" role="main">

      <img src="/img/100px-clojure.png" style="float:left;"/>

      <table>
        <tr>
          <td>Add-on user (retrieved via server-to-server REST):</td>
          <td>{{displayname}}</td>
        </tr>
        <tr>
          <td>Your name (retrieved via <code>AP.request()</code>):</td>
          <td>
            <span id="displayName"></span>
            <span class="aui-icon aui-icon-wait loading">Please wait</span>
          </td>
        </tr>
        <tr>
          <td>This repository (retrieved from callback parameters):</td>
          <td>{{repopath}}</td>
        </tr>
        <tr>
          <td>Page visits:</td>
          <td>
            <span id="pageVisits"></span>
            <span class="aui-icon aui-icon-wait loading">Please wait</span>
          </td>
        </tr>
      </table>

    </section>

    <script src="/js/addon.js"></script>

  </body>
</html>
```
    
This is pretty much a straight port of [Tim Pettersen's][tpettersen]
[NodeJS version Connect example][bb-node-example], but using [Selmer] markup and
adding a Clojure logo. The JavaScript Bitbucket callbacks are implemented in
`/js/addon.js`, which is ripped straight out of Tim's project:

``` javascript
// Bitbucket Connect also supports a client side library - AP (for "Atlassian Plugins") - that
// allows you to interact with the host application. For example, you can make authenticated
// requests to the Bitbucket REST API ...

AP.require('request', function(request) {
    request({
        url: '/1.0/user/',
        success: function(data) {
            $('#displayName')
                .text(data.user.display_name)
                .next('.loading').hide();
        }
    });
});

// ... and set cookies (browser security policies can prevent this from being done in iframes).

var COOKIE_NAME = 'example-visits';

AP.require('cookie', function(cookie) {
    cookie.read(COOKIE_NAME, function(visits) {
        visits = (visits ? parseInt(visits) : 0) + 1;
        cookie.save(COOKIE_NAME, visits, 30);
        $('#pageVisits')
            .text(visits)
            .next('.loading').hide();
    });
});
```

This code in turn utilises the `all-debug.js` library loaded from Bitbucket
that sets up the [web messaging][xdm] between the browser and the server-side.

(At this point, some people are probably asking pointedly why I'm not using
[ClojureScript]. Don't worry, we'll get to that in a later installment, and
maybe some bonus ClojureScript+[core.async] fun too.)

### Server-side API calls

But more interesting for the time being are the server-side calls, so let's get
those working, too. We've already created template markers for variables in the
template above, but we need to retrieve the information. We'll create a new
namespace in `bitbucket.clj` for this. As we're going to be making REST calls
and authenticating we'll pull the HTTP client as a dependency:

``` clojure
(ns hello-connect.bitbucket
  (:require [clojure.tools.logging :as log]

            [clj-http.client :as http]

            [clj-connect.jwt :as jwt]
            [environ.core :refer [env]]
            [hello-connect.storage :as storage]))
```

The JavaScript calls to Bitbucket are automatically authenticated by the
browser, but for the server-side call we'll need to authenticate ourselves. The
recommended method to acquire an OAuth key in Connect is to use the
shared-secret we received during the add-on installation (via the `/installed`
endpoint) to [request an OAuth key from Bitbucket][connect-oauth]. This is done
by making a JWT-authenticated call to Bitbucket:

``` clojure
(defn fetch-oauth-token []
  (let [token (jwt/gen-jwt-token "POST"
                                 "/site/oauth2/access_token"
                                 {}
                                 (env :project-key)
                                 (storage/client-key)
                                 (storage/shared-secret))

        resp (http/post "https://bitbucket.org/site/oauth2/access_token"
                        {:headers {"Authorization" (str "JWT " token)}
                         :form-params {:grant_type "urn:bitbucket:oauth2:jwt"}
                         :as :json})]

    (-> resp
        :body
        :access_token)))
```

(Note that these tokens last 90 minutes so should be cached for a production
setup. However as this is just a demo we can cut corners.)

It is also possible to use the JWT token to authenticate with the API directly,
however this is not fully documented at this point so we'll use the officially
supported OAuth method.

We'll also specify a function to fetch the current user's display-name by
requesting the full information and extracting it with a
[Clojure threading macro][threading]:

``` clojure
(defn bb-display-name [oauth]

  (let [data (http/get "https://bitbucket.org/api/1.0/user/" {:oauth-token oauth
                                                                :as :json})]
    (log/info "Received user data" data)
    (-> data
        :body
        :user
        :display_name)))
```
            
### Serving the UI component up

Now that we have our calls and our template, we can pull it all together by
defining a calling function and route in `handler.clj`:

``` clojure
(defn get-repo-page [request]
  (let [{jwt-token "jwt"
         repo-path "repoPath"} (request :query-params)

         oauth (bitbucket/fetch-oauth-token)
         ctx {:repopath repo-path
              :displayname (bitbucket/bb-display-name oauth)}]
    (log/info "Getting repo information for " repo-path)

    {:status 200
     :headers {"Content-Type" "text/html; charset=utf-8"}
     :body (selmer/render-file "views/connect-example.selmer" ctx)}))

(defroutes app-routes
  (GET  "/" [] (response/redirect "/atlassian-connect.json"))
  (GET  "/atlassian-connect.json" []
        (gen-descriptor-reply))

  (POST "/installed" {params :query-params body :body}
        (process-installed params body))

  (POST "/uninstalled" request
        (wrap-jwt-auth request process-uninstalled))

  (POST "/webhook" request
        (wrap-jwt-auth request process-webhook))

  (GET  "/connect-example" request
        (wrap-jwt-auth request get-repo-page))

  (route/not-found {:status 404 :body "Not Found"}))
```


### Next time

In the previous installments we've created our Connect project, configured the
descriptor and created a REST API. With the addition of a valid UI component to
serve-up in Bitbucket we're ready to take it for a spin. In the next installment
we'll review some of the practicalities of doing that, from both the Clojure and
Bitbucket Connect points of view.


### The code

The code for this part of the tutorial series is available in
[the `part-4` tag][part4-code] in the
[accompanying `hello-connect` Bitbucket repository][hello-connect]. There will
also be code appearing there for the later parts as I work on them if you want
to skip ahead.


[bb-connect-announce]: https://blog.bitbucket.org/2015/06/10/atlassian-connect-for-bitbucket-a-new-way-to-extend-your-workflow-in-the-cloud/
[connect-intro]: https://developer.atlassian.com/bitbucket/guides/introduction.html
[connect-oauth]: https://developer.atlassian.com/bitbucket/concepts/oauth2.html
[JWT]: https://en.wikipedia.org/wiki/JSON_Web_Token
[JWT-qsh]: https://developer.atlassian.com/bitbucket/concepts/understanding-jwt.html
[clj-connect]: https://bitbucket.org/ssmith/clj-connect
[Leiningen]: http://leiningen.org/
[Boot]: http://boot-clj.com/
[Maven]: https://maven.apache.org/
[JDK]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
[Luminus]: http://www.luminusweb.net/
[composable]: https://en.wikipedia.org/wiki/Composability
[lein ancient]: https://github.com/xsc/lein-ancient
[Compojure]: https://github.com/weavejester/compojure
[Ring]: https://github.com/ring-clojure/ring
[Swagger]: http://swagger.io
[12 Factor]: http://12factor.net/
[Jetty]: http://www.eclipse.org/jetty/
[Immutant]: http://immutant.org/
[Undertow]: http://undertow.io/
[s-expressions]: https://en.wikipedia.org/wiki/S-expression
[Handlebars]: http://handlebarsjs.com/
[ERB]: http://apidock.com/ruby/ERB
[Selmer]: https://github.com/yogthos/Selmer
[Django]: https://www.djangoproject.com/
[ngrok]: https://ngrok.com/
[bb-start]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[lein-profiles]: https://github.com/technomancy/leiningen/blob/master/doc/PROFILES.md
[storage.clj]: https://bitbucket.org/ssmith/bitbucket-docker-connect/src/HEAD/src/clojure/docker_connect/storage.clj?at=master
[EDN]: https://github.com/edn-format/edn
[clojure-json]: https://github.com/clojure/data.json
[ring-json]: https://github.com/ring-clojure/ring-json
[clj-jwt]: https://github.com/liquidz/clj-jwt
[bb-docker-connect]: https://bitbucket.org/ssmith/bitbucket-docker-connect
[Bitbucket]: https://bitbucket.org/
[Clojure]: http://clojure.org/
[docker-hub-announce]: https://developer.atlassian.com/blog/2015/09/docker-bitbucket/
[tpettersen]: https://developer.atlassian.com/blog/authors/tpettersen/
[bb-node-example]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[Cider]: https://github.com/clojure-emacs/cider
[Fireplace]: https://github.com/tpope/vim-fireplace
[La Clojure]: https://plugins.jetbrains.com/plugin/4050
[Cursive]: https://cursiveclojure.com/
[descriptor-doc]: https://developer.atlassian.com/bitbucket/descriptor/
[HTTP/2]: https://http2.github.io/
[xdm]: https://en.wikipedia.org/wiki/Web_Messaging
[ClojureScript]: https://github.com/clojure/clojurescript
[core.async]: https://github.com/clojure/core.async
[threading]: https://clojuredocs.org/clojure.core/-%3E
[uberjar]: https://github.com/technomancy/leiningen/blob/master/doc/TUTORIAL.md#uberjar
[AOT]: http://clojure.org/compilation
[curl]: http://curl.haxx.se/
[bitbucket-js]: https://developer.atlassian.com/bitbucket/concepts/javascript-api.html
[closure]: https://en.wikipedia.org/wiki/Google_Closure_Tools
[cljs-rationale]: https://github.com/clojure/clojurescript/wiki/Rationale
[clj-peristent]: https://en.wikipedia.org/wiki/Persistent_data_structure
[cljsbuild]: https://github.com/emezeske/lein-cljsbuild
[Figwheel]: https://github.com/bhauman/lein-figwheel
[hello-connect]: https://bitbucket.org/ssmith/hello-connect/
[hello-connect-descriptor]: https://bitbucket.org/ssmith/hello-connect/src/4810770708e659b6c20dac0e021740f56238dc06/resources/views/atlassian-connect.json.selmer?at=master&fileviewer=file-view-default
[part1]: /blog/2015/12/clojure-connect-part-1/
[part2]: /blog/2015/12/clojure-connect-part-2/
[part3-code]: https://bitbucket.org/ssmith/hello-connect/commits/tag/part-3
[part3]: https://developer.atlassian.com/blog/2016/01/clojure-connect-part-3/
[part4-code]: https://bitbucket.org/ssmith/hello-connect/commits/tag/part-4
